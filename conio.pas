unit conio;
interface

// glowiaks remake of conio.h library for pascal using escape codes

const
    FG_COLOR_BLACK: integer = 30;
    FG_COLOR_RED: integer = 31;
    FG_COLOR_GREEN: integer = 32;
    FG_COLOR_YELLOW: integer = 33;
    FG_COLOR_BLUE: integer = 34;
    FG_COLOR_MAGENTA: integer = 35;
    FG_COLOR_CYAN: integer = 36;
    FG_COLOR_WHITE: integer = 37;

    BG_COLOR_BLACK: integer = 40;
    BG_COLOR_RED: integer = 41;
    BG_COLOR_GREEN: integer = 42;
    BG_COLOR_YELLOW: integer = 43;
    BG_COLOR_BLUE: integer = 44;
    BG_COLOR_MAGENTA: integer = 45;
    BG_COLOR_CYAN: integer = 46;
    BG_COLOR_WHITE: integer = 47;

    COLOR_RESET: integer = 0;

procedure NewLine;
procedure Backspace;
procedure Noise;
procedure ClearScreen;
procedure CleanMarkup;
procedure FastBlink;
procedure Bold;
procedure Italic;
procedure MoveCursor(x: integer; y: integer);
procedure SetColor(color: integer);
procedure ResetColor;
procedure Putch(c: char; x: integer; y: integer);
procedure Cputs(str: string; x: integer; y: integer);

implementation

procedure NewLine;
begin
    write(#10);
end;

procedure Backspace;
begin
    write(#8);
end;

procedure Noise;
begin
    write(#7);
end;

procedure ClearScreen;
begin
    write(#27, '[2J');
end;

procedure CleanMarkup;
begin
    write(#27, '[0m');
end;

procedure FastBlink;
begin
    write(#27, '[6m');
end;

procedure Bold;
begin
    write(#27, '[1m');
end;

procedure Italic;
begin
    write(#27, '[3m');
end;

procedure MoveCursor(x: integer; y: integer);
begin
    write(#27, '[', x, ';', y, 'H');
end;

procedure SetColor(color: integer);
begin
    write(#27, '[', color);
end;

procedure ResetColor;
begin
    SetColor(COLOR_RESET);
end;

procedure Putch(c: char; x: integer; y: integer);
begin
    MoveCursor(x, y);
    write(c);
end;

procedure Cputs(str: string; x: integer; y: integer);
begin
    MoveCursor(x, y);
    write(str);
end;

initialization
end.
