#include <stdio.h>
#include <stdlib.h>

#ifndef CONIO_H
    #define CONIO_H
#endif

#define cprintf printf
#define cscanf scanf

#define PIXEL '@'

// should work on UNIX-like OSes, not on windows

/*
 * glowiak's clone of the <conio.h> library for UNIX-like OSes.
 * made for C, without any "three dee engines or languages with classes and garbage collection and namespaces and things you don't really need" (citate from jdh "Making Minecraft in 48 hours [NO GAME ENGINE]")
 * 
 * Implements functions:
 * 
 * void clrscr() - clear screen
 * void gotoxy(const int x, const int y) - title says its all
 * void textcolor(const int color) - change color of the text
 * void textbackground(const int color) - change background color
 * char getch() - get pressed key without need to press enter
 * void getche() - the same as previous, but prints output to the screen
 * void putch(const char c, const int x, const int y) - put character in specified location
 * void cputs(const char *str, const int x, const int y) - put a string in specified location
 * int kbhit(const char key) - check if a key is pressed (0 = true; 1 = false)
 * 
 * My custom functions:
 * 
 * void newLine() - insert newline character (totally not needed)
 * void backspace() - delete a character
 * void noise() - make a noise
 * void blink() - make text blink
 * void bold() - make text bold
 * void italic() - make text italic
 * void putpixel(int color, int x, int y) - draw colorful pixels. the pixel character is defined in PIXEL
 * void cleanMarkup() - clean all markup and colors
 * void putcoloredtext(const char *str, const int color, const int x, const int y) - like cputs, but with colors
 */

const int BLACK = 30;
const int RED = 31;
const int GREEN = 32;
const int YELLOW = 33;
const int BLUE = 34;
const int MAGENTA = 35;
const int CYAN = 36;
const int WHITE = 37;

int currentColor = 0;

void newLine()
{
    printf("\n");
}

void backspace()
{
    printf("\b");
}

void noise()
{
    printf("\a");
}

void clrscr()
{
    printf("[2J");
}

void cleanMarkup()
{
    printf("[0m");
}

void blink()
{
    printf("[6m");
}

void bold()
{
    printf("[1m");
}

void italic()
{
    printf("[3m");
}

void gotoxy(const int x, const int y)
{
    printf("[%d;%dH", y, x);
}

void textcolor(const int color)
{
    currentColor = color;
    printf("\033[%dm", color);
}

void textbackground(const int color)
{
    if (color != 0)
    { textcolor(color + 10); }
    else { textcolor(0); }
}

char getch()
{
    char c;
    system("stty raw -echo");
    c = getchar();
    system("stty -raw echo");
    return c;
}

void getche()
{
    putchar(getch());
}

void putch(const char c, const int x, const int y)
{
    gotoxy(x, y);
    printf("%c", c);
}

void cputs(const char *str, const int x, const int y)
{
    gotoxy(x, y);
    printf(str);
}

int kbhit(const char key)
{
    char c = getch();
    
    if (c == key) { return 0; } else { return 1; }
}

void putpixel(const int color, const int x, const int y)
{
    int lastColor = currentColor;
    textcolor(color);
    putch(PIXEL, x, y);
    textcolor(lastColor);
}

void putcoloredtext(const char *str, const int color, const int x, const int y)
{
    int lastColor = currentColor;
    textcolor(color);
    cputs(str, x, y);
    textcolor(lastColor);
}
